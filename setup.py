from setuptools import setup, find_packages

setup(name='deriskdatabase',
      version='0.1',
      description='Library to work with the DeRisk Database files',
      author='Fabio Pierella',
      author_email='fabpi@dtu.dk',
      url='https://data.dtu.dk/articles/The_DeRisk_Database/10322033',
      packages=['deriskdatabase'],
     )
