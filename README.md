# The DeRisk Database

## Introduction

The DeRisk database is an extensive database of long-crested (2D) nonlinear wave kinematics for computation of extreme loads on offshore structures.

The database was produced by running the nonlinear potential flow model OceanWave3D [1] on a gently sloping domain. The performed computations spanned a large parameter space, defined by nondimensional water depth, significant wave height and spectral peak period. A work containing a thorough description and validation of the current computations and of the database concept is currently under preparation [2].

Here we present some software routines for interacting with the DeRisk database.

## The Database

The database contains 80 simulations. We used 8 different combinations of 
significant wave heights $`H_S`$ and peak periods $`T_P`$.
For each combination, 10 different random seeds were used.
For each of the 80 simulations, the wave kinematics was sampled at 120 points, for a total of 960 sampled locations.
For each of the locations, circa 6 hours of simulated time series (after excluding the initial transients) are available, for a total of 57600 hours of simulated time.

## Installation

```
python setup.py install
```

## Usage

The usage is intended to be quite simple.
A user comes with a target sea state, with significant wave height $`H_S`$, a peak period $`T_P`$ and a depth $`h`$ for which he needs to produce nonlinear kinematics.

The process consists in two main steps:
- Find out which simulation in the DataBase is nondimensionally similar with the target sea state
- Extracting the kinematics and disregardin the transient
- Scaling the kinematics to match the taget sea state scale

See example folder.

## Citations

[1] Engsig-Karup, Allan Peter, Harry B. Bingham, and Ole Lindberg. "An efficient flexible-order model for 3D nonlinear water waves." Journal of computational physics 228.6 (2009): 2100-2118.

[2] Pierella, F., Bredmose, H., Read, R., Lindberg, O., Bingham, H.B., Engsig-Karup, A.P. "The DeRisk Database: extreme design waves for Offshore Wind Turbines." https://arxiv.org/abs/2012.02839

