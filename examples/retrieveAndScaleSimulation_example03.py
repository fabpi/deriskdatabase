import deriskdatabase as db
import pylab as plt
import h5py

DB = db.database(path='__sims__etax__')
db.plotDatabase(DB, tp_metric='Tp')

# Find out the important simulations
simFolder, simZone, simIndex = db.extractSimulationFromDatabase(DB, 
    Hs=8., Tp=12., depth=20.,
    strategy='N_best', N_points=1, tp_metric='Tp')
    
# Write files relative to these test cases
db.writeKinematicsFiles(DB, simFolder, simZone, simIndex, 
    format='h5', scaling='depth', 
    timeSeriesLength = 10800., dt = 0.1,
    depth=20., outputFolder='output')
