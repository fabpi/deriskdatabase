#!/usr/bin/env python3
import deriskdatabase as db
import deriskdatabase.force as fc
import pylab as plt
import h5py
import numpy as np

plt.close('all')

# Constants
GRAVITY = 9.80665 # ms**-2
RHO = 1025. # kgm**-3

# Force computations Parameters
Cm = 1.0 # Added mass coefficient
CM = 1.0 + Cm # Mass coefficient
CD = 0.5 # Drag coefficient
D = 7.0 # [m]
h = 20. # [m]

fid = h5py.File('output/Kinematics00.h5','r')

# Force with Morison Equation
t = fid['time'][:]
t = t-t[0] # Time starting from zero
z = fid['position_z'][:]

eta = fid['surface_elevation'][:]
etax = fid['surface_elevation_derivative_etax'][:]

u = fid['velocity_u'][:]
ut = fid['velocity_derivative_ut'][:]
ux = fid['velocity_derivative_ux'][:]
uz = fid['velocity_derivative_uz'][:]

w = fid['velocity_w'][:]
wz = fid['velocity_derivative_wz'][:]

# Total derivative: we need it for the Rainey force model
utot = ut + u*ux + w*uz

Fx_rainey, _, My_rainey, _ = fc.load_on_cylinder(t, z,  D, u, utot, eta, h, 
                wz=wz, etax=etax,
                cm = CM, cd=CD, rhoWater=RHO, useRainey=True)

Fx_morison, _, My_morison, _ = fc.load_on_cylinder(t, z,  D, u, ut, eta, h, 
                wz=wz, etax=etax,
                cm = CM, cd=CD, rhoWater=RHO, useRainey=False)


# Force
plt.figure()
plt.plot(t, Fx_rainey, label='rainey')
plt.plot(t, Fx_morison, label='morison')
plt.title(r'$F_x$')
plt.xlabel(r'$t[s]$')
plt.ylabel(r'$F[N]$')
plt.legend()
plt.grid()

# Moment
plt.figure()
plt.plot(t, My_rainey, label='rainey')
plt.plot(t, My_morison, label='morison')
plt.title(r'$M_y$')
plt.xlabel(r'$t[s]$')
plt.ylabel(r'$F[N]$')
plt.legend()
plt.grid()

# Rainey vs. Morison
plt.figure()
plt.plot(Fx_morison, Fx_rainey, '.')
plt.xlabel(r'$F_x$ Morison'); plt.ylabel(r'$F_x$ Rainey')
plt.title(r'Morison vs. Rainey $F_x$')
plt.grid()

plt.figure()
plt.plot(My_morison, My_rainey, '.')
plt.xlabel(r'$M_y$ Morison'); plt.ylabel(r'$M_y$ Rainey')
plt.grid()
plt.title(r'Morison vs. Rainey $M_y$')

plt.show()
