import numpy as np
import h5py
import os
import pylab as plt
import logging

plt.close('all')

# Change with your location of database (e.g. D:\DeRiskDatabase\)
database_folder = '__sims__etax__' 
# I just selected one of the folders of the database
simulation_folder = 'e43382'

# the full path to the spectrum file
spectrumFile = os.path.join(database_folder, simulation_folder, 'eta0_coeffs')

freq, _, Real, Imag = np.loadtxt(spectrumFile, skiprows = 1, unpack=True)

phase = np.angle(Real + 1j*Imag) # random phases for the particular one-sided spectrum
amplitude = np.abs(Real + 1j* Imag) # amplitude of the one-sided spectrum

plt.figure()
plt.plot(freq, amplitude, 'r-')
plt.xlabel(r'$f[Hz]$')
plt.ylabel(r'$A[m]$')
plt.grid(True)
plt.xlim([0,1])
plt.show()
