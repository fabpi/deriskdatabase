import pandas as pd
import os
import pylab as plt
from deriskdatabase.functions import breaking_limit
import numpy as np

def adjust_plot():
    # Adjustments to make the plot look nice
    plt.xlabel(r'$h/(gT_P^2)$')
    plt.ylabel(r'$H_S/(gT_P^2)$')
    plt.ylim([0.001, 0.01])
    plt.xlim([0.001, 0.1])
    plt.grid(True, which='both')
    plt.tight_layout()
    plt.legend()

plt.close('all')
# Change with your location of database (e.g. D:\DeRiskDatabase\)
database_folder = 'DeRiskDatabase' 
resume_file = os.path.join(database_folder, 'resume.csv')

# Load the database resume file
DB = pd.read_csv(resume_file)

# Create and show a new figure
plt.figure()
plt.loglog(DB['h_star'], DB['Hs_star'], '.')

# Compute the breaking limit curve
T = 10. # [s]
g = 9.80665 # ms**-2
h = np.linspace(1., 200., 100)
H_breaking = np.zeros_like(h)
for i,h_ in enumerate(h):
    H_breaking[i] = breaking_limit(H=0., depth=h_, T=T, g=g)
plt.loglog(h/(g*T**2), H_breaking/(g*T**2), label='Breaking Limit')
adjust_plot()

# For this part of the example, you need seaborn == 0.9.0
import seaborn as sns
plt.figure()
cp = sns.color_palette('deep', 8)
splot = sns.scatterplot(data=DB, x='h_star', y='Hs_star', hue='Hs_nom', 
    palette=cp, marker='.')
splot.set(xscale="log", yscale="log")
plt.loglog(h/(g*T**2), H_breaking/(g*T**2), label='Breaking Limit')
adjust_plot()

plt.show()
