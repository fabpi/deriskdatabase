import h5py
import os
import pylab as plt
import logging

plt.close('all')

# Change with your location of database (e.g. D:\DeRiskDatabase\)
database_folder = 'DeRiskDatabase' 
# I just selected one of the folders of the database
simulation_folder = 'e43382'

# the full path to WaveKinematicsZone001.h5
hdf5_file = os.path.join(database_folder, simulation_folder, 'WaveKinematicsZone001.h5')

fid = h5py.File(hdf5_file, 'r')

# Print the fields that are stored in the file
logging.info('Fields in the HDF5 file:')
for k in fid.keys():
    logging.info(k)

# Save some fields
t = fid['time'][:]
eta = fid['surface_elevation'][:]
z = fid['position_z'][:]

# Close the file
fid.close()

# We inspect the dimensionality of the fields
logging.info('Shape of time array (t): {}'.format(t.shape))
logging.info('Shape of free surface elevation array (t,x,y): {}'.format(eta.shape))
logging.info('Shape of z position of sampling points (t,x,y,z): {}'.format(z.shape))

plt.figure()
# Plot the free surface elevation:
plt.plot(t, eta[:, 0, 0], 'r.')
# Plot the z-coordinate of sampling points
plt.plot(t, z[:, 0, 0, :], 'k-', )

plt.xlabel(r'$t[s]$'); plt.ylabel(r'$z[m]$')
plt.xlim([6000, 6100])
plt.legend([r'$\eta(t,x)$', r'$z(t,x,z)$'], loc=0)
plt.grid(True)
plt.show()
