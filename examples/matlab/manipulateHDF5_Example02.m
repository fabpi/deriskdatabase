% Example 02
% DeRisk Database Workshop 

DBfolder = 'E:\DeRiskDatabase\';
Sim = '0a7683';
File = 'WaveKinematicsZone001.h5';

% Display the datasets in the file
fields = h5info(fullfile(DBfolder, Sim, File));
fields.Datasets.Name

% Read the data into MATLAB arrays
t = h5read(fullfile(DBfolder, Sim, File),'/time');
eta = h5read(fullfile(DBfolder, Sim, File),'/surface_elevation');
z = h5read(fullfile(DBfolder, Sim, File),'/position_z');

% Plot the results
figure()
plot(t, squeeze(eta(1,1,:)), 'o')
ylabel('$\eta[m]$', 'interpreter','latex')
xlabel('$t[s]$', 'interpreter','latex')
grid on
hold on
plot(t, squeeze(z(:,1,1,:)),'k')
xlim([5500, 5600])