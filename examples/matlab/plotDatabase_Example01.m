% Example 01 
% DeRisk Database Workshop 

DB = readtable('E:\DeRiskDatabase\resume.csv');

figure
plot(DB.h_star, DB.Hs_star, '.')
xlabel('$h/(gT_P^2)$', 'interpreter','latex')
ylabel('$H_S/(gT_P^2)$', 'interpreter','latex')
grid on
