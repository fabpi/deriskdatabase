import logging
import numpy as np

def load_on_cylinder(time, z,  D, u, ut, eta, depth, 
                    wz=None, etax=None, v=None, vt=None, etay=None, 
                    cm = 2., cd=0.7, rhoWater=1025., useRainey=True):

    """ Calculates the Static force on a cylinder,
    arising from a timeseries of velocity and acceleration.
    It uses the trapezoid integration at every timestep.
    Note: both z and sigma f need to go from smallest (index 0)
    to larges (index -1).
    This is because index -1 is used to calculate the surface velocity.
    
    
    Parameters
    ----------
    time : ndarray of floats
        Time array [s]
    z : ndarray of floats
        Vertical positions, at which the kinematics is calculated [m]
    D : float
        Diameter of the cylynder [m]
    u : ndarray of floats
        Velocity array (x-wise) [m/s]
    ut : ndarray of floats
        Total (material) acceleration array [m/s] 
    eta : ndarray of floats
        Wave elevation array [m]
    depth : float
        Water depth [m]
    v : ndarray of floats
        Velocity array (y-wise) [m/s] (default=None)
    vt : ndarray of floats
        Velocity array (y-wise) [m/s] (default=None)
    wz : ndarray of floats
        Vertical velocity gradient [m/s] (default=None)
    etax : ndarray of floats
        x-wise gradient of wave elevation [-] (default=None)
    etay : ndarray of floats
        y-wise gradient of wave elevation [-] (default=None)
    cm : float 
        Added mass coefficient (default=2.)
        NOTE: it will be considered equal for x and y direction.
    cd : float
        Drag coefficient (default=0.7)
    rhoWater : float
        Density of the water (default=1025.)
    useRainey : bool
        Whether to use Rainey force (default=True). 
        If False, the Morison load model is used.
    
    Returns
    --------
    Fx : ndarray of floats
        Force array (x-wise) [N]
    Fy : ndarray of floats
        Force array (y-wise) [N]
    My : ndarray of floats
        Moment array (pitch) [Nm]
    Mx : ndarray of floats 
        Moment array (roll)  [Nm]
    
    """

    # checks of the input parameters
    if useRainey:
        Rainey = 1.
        if wz is None:
            raise ValueError('The Rainey Force model requires a vertical velocity gradient input')
        if etax is None:
                raise ValueError('The Rainey Force model requires a wave elevation gradient input')
    elif not useRainey:
        Rainey = 0.
        if wz is None:
            wz = np.zeros_like(u)    
        if etax is None:
            etax = np.zeros_like(eta) 
            
    if (z[0,0] > z[0,-1]):
        raise ValueError('ERROR: The z needs to be increasing.')
        
    if v is None:
        v = np.zeros_like(u)
        logging.warning('WARNING: v is being set to zero')
 
    if vt is None:
        vt = np.zeros_like(v)
        logging.warning('WARNING: vt is being set to zero')
        
    if etay is None:
        etay = np.zeros_like(time)
        logging.warning('WARNING: etay is being set to zero')   
   
    Nz=len(u[0,:])
    
    Area=(np.pi/4)*D**2
        
    fwave=np.zeros((len(time),Nz)) # Wave force per meter    
    fwavey=np.zeros((len(time),Nz)) # Wave force per meter    
    
   
    fwave=cm*rhoWater*Area*ut + \
            0.5*rhoWater*cd*D*u*np.sqrt(u**2+v**2)+ \
            rhoWater*(cm-1)*Area*wz*u*Rainey # axial divergence term

    fwavey=cm*rhoWater*Area*vt + \
            0.5*rhoWater*cd*D*v*np.sqrt(u**2+v**2)+ \
            rhoWater*(cm-1)*Area*wz*v*Rainey # axial divergence term

    # Surface velocity for the point force at surface intersection
    u_surf = u[:,-1]; v_surf = v[:,-1]

    # Point force at the intersection
    Pointforce=-0.5*rhoWater*Area*(cm-1)*etax*u_surf**2*Rainey
    Pointforcey=-0.5*rhoWater*Area*(cm-1)*etay*v_surf**2*Rainey    
   
    # x-wise force
    FTx=np.trapz(fwave, z)
    FTx=FTx+Pointforce
    mwave=(z+depth)*fwave
    # moment around y axis: positive x-wise force induces positive y-wise moment
    MTy=np.trapz(mwave, z) + Pointforce*(depth+eta)

    # y-wise force
    FTy=np.trapz(fwavey, z)
    FTy=FTy+Pointforcey
    mwavey=(z+depth)*fwavey

    # y-wise moment: a positive y-force induces a negative x-wise moment
    MTx= -np.trapz(mwavey, z) - Pointforcey*(depth+eta)

    return FTx, FTy, MTy, MTx
