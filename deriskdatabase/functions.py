import os
import copy
import pandas as pd
import pylab as plt
import logging
import h5py
import scipy.io as sio
import sys
import numpy as np
import scipy.fftpack as sfft
import zipfile as zf
import ipdb
import glob

GRAVITY = 9.80665 # ms**-2

class database():
    """ Object database with all the database parameters:
    - Absolute path where the simulations are (absolutePath)
    - Relative path where the simulations are (moldPath)
    """
    def __init__(self, path, Beta=0.5, isZipped=False):
        """
        Parameters:
        -----------
        path: str
            Path where the database is

        """

        self.path = os.path.join(path)

        # This is a resume file of the database.
        # To compute the Hs and Tp, an initial portion of 6000 [s] was disregarded.
        # However, the data is still in its original form.
        self.batchResPath = os.path.join(self.path,'resume.csv')
        self.df = pd.read_csv(self.batchResPath)
        # Sometimes it might be easier to keep the database in zipped form.
        # Here we introduce a flag to signal we do not want to extract it
        self.isZipped = isZipped

        self.list_of_present_simulations = []
        if self.isZipped:
            zipFiles = glob.glob(os.path.join(self.path, '*.zip'))
            self.list_of_present_simulations = [os.path.splitext(os.path.basename(x))[0] for x in zipFiles]
        else:
            self.list_of_present_simulations = next(os.walk(self.path))[1]


def plotDatabase(dataBase, tp_metric='Tp', tLenMin = 9600., **kwargs):
    """ Simple function to print the states in the database
    tp_metric: string
        How to measure the time scale. Can be Tp (peak period, default) or T12
        (square root of ratio of zeroth-moment of the spectrum over second-moment)
    tLenMin: float
        Minimum length of the time series that will be plotted.
        The first 6000. [s] are the transitory, and we'd like to plot
        time series that are at least 1hr long. Hence the default value of 9600 [s].

    """
    # Select the simulations wtih a suitable Hs_star
    # Reset the index of the pandas dataframe
    df = dataBase.df
    # Disregard too short simulations, where the parameters are difficult to extract
    df = df[df.tLen > tLenMin]

    df = df.reset_index()

    ax = printDots(Hs_star_target=None, h_star_target=None,
        sims_in_range=None, sims_not_in_range=df, tp_metric=tp_metric)

    ax.get_legend().remove()

    logging.info('Length of database: {}'.format(len(df)))
    logging.info('Total time of database: {}'.format((df['tEnd']-6000.).sum()))

    return ax

def printDots(Hs_star_target=None, h_star_target=None, 
    sims_in_range=None, sims_not_in_range=None,
    tp_metric='Tp'):
    """ Print a visualization of the database on nondimensional
    depth and Hs axes.

    Parameters:
    -----------
    Hs_star_target: float
        Target nondimensional significant wave height Hs.
        If present, this particular sea state will be printed as a red cross.
    
    h_star_target: float
        Target nondimensional depth h.
        If present, this particular sea state will be printed as a red cross.    

    tp_metric: str
        How to measure the time scale. Can be Tp (peak period, default) or T12
        (square root of ratio of zeroth-moment of the spectrum over second-moment)        
    
    sims_in_range: pandas dataframe
        Simulations that will be plotted with a filled marker
    
    sims_in_range: pandas dataframe
        Simulations that will be plotted with a transparent marker        
    """


    _, axDots = plt.subplots()
    # axDots.set_prop_cycle(custom_cycler)
    try:
        axDots.plot(h_star_target, Hs_star_target, 'r+', markersize=10, markeredgewidth=2.5, label='Exp')
    except Exception as e:
        logging.error(e)

    
    try:
        if tp_metric == 'Tp':
            simh_star = sims_in_range['h_star']
            simHs_star = sims_in_range['Hs_star']
        elif tp_metric == 'T12':
            simh_star = sims_in_range['h'] / (GRAVITY*sims_in_range['T12']**2)
            simHs_star = sims_in_range['Hs'] / (GRAVITY*sims_in_range['T12']**2)
        
        axDots.plot(simh_star, simHs_star, 'o', color='teal', label='Selected')
    except Exception as e:
        logging.error(e)

    try:
        if tp_metric == 'Tp':
            simh_star = sims_not_in_range['h_star']
            simHs_star = sims_not_in_range['Hs_star']
        elif tp_metric == 'T12':
            simh_star = sims_not_in_range['h'] / (g*sims_not_in_range['T12']**2)
            simHs_star = sims_not_in_range['Hs'] / (g*sims_not_in_range['T12']**2)

        axDots.plot(simh_star, simHs_star, '.', color='teal', label='Not selected', alpha=.33)
    except Exception as e:
        logging.error(e)

    axDots.grid(True); axDots.legend(loc=2)
    axDots.set_xlabel(r'$h/(g \cdot T^2)$')
    axDots.set_ylabel(r'$H_S/(g \cdot T^2)$')      

    return axDots

def extractSimulationFromDatabase(dataBase, 
        Hs=8., Tp=12., depth=20.,
        strategy='N_best', **kwargs):
    """ Given a certain nondimensional Hs and h, extract the 5 closes points
    from the database, according to the various philosophies:

    - Ellipse
    - Closest points

    Choose where scaling should be scaled on:
    - Peak period
    - Depth

    Parameters:
    -----------
    dataBase : database object
        Object with all the details on the database
    Hs : float
        Significant wave height [m]
    Tp : float
        Peak period [s]
    depth : float
        Depth [m]
    
    strategy : str
        Strategy for selecting. Can be N_best or Ellipse

    Optional parameters:
    --------------------
    ellipse_h_axis: float
        If strategy is Ellipse, 
        specify length of the ellipse in the nondimensional h (horizontal) axis          
    ellipse_Hs_axis: float
        If strategy is Ellipse,
        specify length of the ellipse in the nondimensional Hs (vertical) axis
    N_points: int
        If the strategy is "N_points", then specify N_points as optional keyword

    Returns:
    --------
    sims_selected: list of OW3DSimulation objects
        List of selected simulations
    sims_selected_zones: list of int
        List of selected zones for comparison
    sims_selected_ix: list of int
        List of indices of sampling points in the zone

    """
    tLenMin = 9600. # Hardcode the minimum length of sim that I want to use

    h_star_target = depth/(GRAVITY*Tp**2)
    Hs_star_target = Hs/(GRAVITY*Tp**2)

    # Select the simulations wtih a suitable Hs_star
    # Reset the index of the pandas dataframe
    df = dataBase.df
    # Disregard too short simulations, where the parameters are difficult to extract
    df = df[df.tLen > tLenMin]

    # Filter by simulations that are actually there
    isPresentFilter = df['simPath'].apply(lambda x: True if x in dataBase.list_of_present_simulations else False)
    df = df[isPresentFilter]

    df = df.reset_index()

    # Initialize empty lists
    sims_selected = []
    sims_selected_zones = []
    sims_selected_ix = []

    # Select the way you want to compute the time scale
    simh_star = df['h_star']
    simHs_star = df['Hs_star']
   
    # Use the strategy to select the closest points
    if strategy == 'N_best':
        # When we choose N_best points, we also want to input how many
        N_points = kwargs.get('N_points', 5)        
        # "Closeness" method those in range
        distance = ((h_star_target-simh_star)**2 + (Hs_star_target-simHs_star)**2)**0.5
        F = distance.argsort()
        # To start with, select 50 simulations
        sims_in_range = df.loc[F][:50]

    elif strategy == 'Ellipse':
        # "Ellipse" method
        h_axis = kwargs.get('ellipse_h_axis', 2.5e-4)        
        Hs_axis = kwargs.get('ellipse_Hs_axis', 3.5e-5)        
        ax1 = (h_star_target-simh_star)/h_axis
        ax2 =  (Hs_star_target-simHs_star)/Hs_axis
        F = (ax1**2+ax2**2)<1.
        sims_in_range = df.loc[F]

    # We only want one simulation from each "batch" to make it in the 
    # extracted results.
    # This is to avoid that two signals come from the same set of phases.
    unique_sims_in_range = []
    ix_unique_sims_in_range = []
    unique_sims_in_range.append(sims_in_range.iloc[0]) # stack the first simulation

    # Start with the closest simulation [0]
    ix_unique_sims_in_range.append(sims_in_range.iloc[0].name)
    for i,s in sims_in_range.iloc[1:].iterrows(): # enumerate the other sims
        if not (s.simPath in [l.simPath for l in unique_sims_in_range]):
            unique_sims_in_range.append(s)
            ix_unique_sims_in_range.append(i)                        
        else:
            continue # skip them if already in the list
            
    if strategy == 'N_best':
        # now get only the closest 5 among the unique ones, if strategy is N_best
        sims_in_range = sims_in_range.loc[ix_unique_sims_in_range[:N_points]]

    # disregard the other ones
    sims_not_in_range = df[~df.isin(sims_in_range)].dropna()

    # Print a visualization of the chosen points
    printDots(Hs_star_target, h_star_target, 
        sims_in_range=sims_in_range, 
        sims_not_in_range=sims_not_in_range, 
        tp_metric='Tp')

    for _, ss in sims_in_range.iterrows():
        # The batchResume.pkl file comes with an extra part in the simulation path.
        # Needs to be split in two
        sims_selected.append(ss.simPath)
        sims_selected_zones.append(ss['zone'])
        sims_selected_ix.append(ss['ix'])

    # sims_selected contains the object reference to the simulation
    logging.info('Selected {} simulations'.format(len(sims_selected)))

    return sims_selected, sims_selected_zones, sims_selected_ix

def smooth_runmean(s, N, norm=False):
    """ Running mean to smooth an array

    Parameters
    ----------
    s: ndarray of floats
        Array to be smoothed
    N: int
        Number of adjacent samples for running mean
    norm: bool
        Normalize the array by the maximum absolute value (def False)


    Returns
    -------

    arraySm: ndarray of floats
        Smoothed array
    """

    smoothed_sp = np.convolve(s, np.ones((N,))/N, mode='same')

    if norm:
        smoothed_sp = smoothed_sp/np.max(np.abs(smoothed_sp))
    
    return smoothed_sp

def psd(s, dt, N=1):
    """ Calculate a the one-sided amplitude and psd spectrum.
    x-axis is in Hertz.
    
    Parameters:
    -----------
    s : ndarray of float
        Signal
    dt : float
        Time delta of the signal [s]
    N : int
        Number of samples for smoothing 
        
    Returns:
    --------
    f1s : ndarray of floats
        Two-sided frequency array [Hz]
    psdx : ndarray of floats
        Power Spectral Density [(unit)/Hz]
    a1s : ndarray of floats
        Amplitudes for the 2-sided spectrum
        
    """
    # Speedup the algorithm by checking the next fast FFT length
    # ATTENTION: it will pad the signal with zeros!!!
    lenS = sfft.next_fast_len(len(s))
    
    f2s = np.fft.fftfreq(lenS, dt)
    df = f2s[1]-f2s[0]

    sigfft = np.fft.fft(s, n=lenS)

    # Amplitude 1-sided spectrum
    # we start with the 
    a2s = np.abs(sigfft)/lenS

    # Create the two-sided spectra
    if lenS%2 == 0:
        a1s = np.zeros(lenS//2+1)
        a1s = a2s[0:lenS//2+1]*2
        a1s[0] *= 0.5 # zero component is not folded
        a1s[-1] *= 0.5 # nyquist component was already folded            

        f1s = np.zeros(lenS//2+1)
        f1s = f2s[0:lenS//2+1]
        f1s[-1] *= -1 # Python gives a negative Nyquist freq.

    if lenS%2 !=0:
        a1s = np.zeros(lenS//2+1)
        a1s = a2s[0:lenS//2+1]*2
        a1s[0] *= 0.5 # zero component is not folded
        # the last component of the a1s also needs folding, so no correction

        f1s = np.zeros(lenS//2+1)
        f1s = f2s[0:lenS//2+1]

       
    psdx = a1s**2/(2*df)                 
    psdx = smooth_runmean(psdx, N=N)
        
    return f1s, psdx, a1s

def openKinematicsFile(dataBase, record, zone):
    """ Open the kinematics file for reading
    """
    try:
        if dataBase.isZipped:        
            fname = os.path.join(
                dataBase.path, # base path
                record['simPath'][0]+'.zip') # particular simulation path
            zipFname = zf.ZipFile(fname)        
            
            # Look for the hdf5 file inside the zipped folder
            h5File = ('WaveKinematicsZone{:03d}.h5'.format(zone)) # which zone                
            fiz = zipFname.open(os.path.join(record['simPath'][0], h5File))

            # Open an handle to the zipped file
            fid = h5py.File(fiz,'r')
            
        else:
            # Find the correct file and open it
            h5File = os.path.join(
                dataBase.path, # base path
                record['simPath'][0], # particular simulation path
                'WaveKinematicsZone{:03d}.h5'.format(zone)) # which zone

            # Open the file and save some important details
            fid = h5py.File(h5File, 'r')
    except:

        # Do some cleanup in case  some files are left open by mistake
        try:
            fiz.close()
        except:
            pass

        try:
            fid.close()
        except:
            pass
        raise
        

    return fid

def writeKinematicsFiles(dataBase, 
    simSelected, zoneSelected, ixSelected,
    format='mat', scaling='depth', outputFolder = 'output', 
    timeSeriesLength = 10800., dt = 0.1,
    **kwargs):
    """ Write the database results into a convenient format
    dataBase : database object
        Object with all the details on the database
    simSelected: list of strings
        Simulations that were selected by extractSimulationFromDatabase
    zoneSelected: list of int
        Zones that were selected by extractSimulationFromDatabase
    ixSelected: list of int
        Indices that were selected by extractSimulationFromDatabase
    timeSeriesLength : float
        The max length of the time series of output values you want to output
    dt : float
        New timestep for the interpolated array
        
    format: str
        Can be:
        - 'mat': matlab binary file
        - 'h5': HDF5 file
    scaling : str
        What to use to scale the simulation. It can be:
        depth | Hs | Tp | T12

        It needs an additional keyword parameter, either:
        'depth', 'Hs', 'Tp', 'T12'

    """

    # tMin : Warmup time in the simulations that we want to disregard [s]. 
    # Hardcoded to 6000 [s].
    tMin = 6000.

    os.makedirs(outputFolder, exist_ok=True)

    allResults = []

    for i, (sim, zone, ix) in enumerate(
                                    zip(simSelected, 
                                    zoneSelected,
                                    ixSelected)
                                    ):

        # Filter out the right simulation
        F1 = dataBase.df['simPath'] == sim
        F2 = dataBase.df['zone'] == zone
        F3 = dataBase.df['ix'] == ix
        
        record = dataBase.df[F1&F2&F3].reset_index()

        logging.info('Analyzing simulation in folder {}'.format(sim))

        fid = openKinematicsFile(dataBase, record, zone)

        tSim = fid['time'][:]
        hSim = record['h'][0] # Depth at which sampling took place

        # Filter and remove the transient (up to tMin)
        FtMin = tSim >= tMin # bool array
        etaSim = fid['surface_elevation'][FtMin,ix,0]

        # Find out the Tp. Smoothed out.
        fSim, psdSim, _ = psd(etaSim, dt=tSim[2]-tSim[1], N=100)
        TpSim = fSim[psdSim.argmax()]**-1 # Inverse of the f at which psd is max

        # Determine the scaling
        if scaling == 'depth':
            depth = kwargs.get('depth', None)
            Lambda = depth / hSim
        elif scaling == 'Hs':
            Hs = kwargs.get('Hs', None)
            Lambda = Hs/(4*np.std(etaSim))
        elif scaling == 'Tp':
            Tp = kwargs.get('Tp', None)
            Lambda = Tp**2/TpSim**2
        else:
            raise ValueError

        # Now that we have computed a scaling factor, we can 
        # save a dictionary with all the data.

        result = {} # We store the scaled results in this dictionary

        result['time'] = tSim[FtMin]*Lambda**0.5
        result['time'] = result['time'] - result['time'][0]
        result['surface_elevation'] = etaSim*Lambda

        result['velocity_u'] = fid['velocity_u'][FtMin, ix, 0, :]*Lambda**0.5
        result['velocity_v'] = fid['velocity_v'][FtMin, ix, 0, :]*Lambda**0.5
        result['velocity_w'] = fid['velocity_w'][FtMin, ix, 0, :]*Lambda**0.5

        result['position_x'] = fid['position_x'][FtMin, ix, 0, :]*Lambda
        result['position_y'] = fid['position_y'][FtMin, ix, 0, :]*Lambda
        result['position_z'] = fid['position_z'][FtMin, ix, 0, :]*Lambda

        result['velocity_derivative_ut'] = fid['velocity_derivative_ut'][FtMin, ix, 0, :]
        result['velocity_derivative_vt'] = fid['velocity_derivative_vt'][FtMin, ix, 0, :]
        result['velocity_derivative_wt'] = fid['velocity_derivative_wt'][FtMin, ix, 0, :]

        result['velocity_derivative_ux'] = fid['velocity_derivative_ux'][FtMin, ix, 0, :]/Lambda**0.5
        result['velocity_derivative_vx'] = fid['velocity_derivative_vx'][FtMin, ix, 0, :]/Lambda**0.5
        try:
            result['velocity_derivative_wx'] = fid['velocity_derivative_wx'][FtMin, ix, 0, :]/Lambda**0.5
        except KeyError:
            result['velocity_derivative_wx'] = fid['velocity_derivative_uz'][FtMin, ix, 0, :]/Lambda**0.5

        result['velocity_derivative_uy'] = fid['velocity_derivative_uy'][FtMin, ix, 0, :]/Lambda**0.5
        result['velocity_derivative_vy'] = fid['velocity_derivative_vy'][FtMin, ix, 0, :]/Lambda**0.5
        try:
            result['velocity_derivative_wy'] = fid['velocity_derivative_wy'][FtMin, ix, 0, :]/Lambda**0.5
        except KeyError:
            result['velocity_derivative_wy'] = fid['velocity_derivative_vz'][FtMin, ix, 0, :]/Lambda**0.5

        result['velocity_derivative_uz'] = fid['velocity_derivative_uz'][FtMin, ix, 0, :]/Lambda**0.5
        result['velocity_derivative_vz'] = fid['velocity_derivative_vz'][FtMin, ix, 0, :]/Lambda**0.5
        result['velocity_derivative_wz'] = fid['velocity_derivative_wz'][FtMin, ix, 0, :]/Lambda**0.5

        result['surface_elevation_derivative_etax'] = fid['surface_elevation_derivative_etax'][FtMin, ix, 0]
        result['surface_elevation_derivative_etay'] = fid['surface_elevation_derivative_etay'][FtMin, ix, 0]

        fid.close()

        allResults.append(result)

    # Handy function to interpolate 2D arrays
    def interp2D(res, field):
        return np.array(
            [np.interp(tNew, tOld, res[field][:,i]) for i in range(res[field].shape[1])]
        ).T

    # Now interpolate all the old arrays on the new timebase
    for res in allResults:
        # Interpolate in new time basis
        tOld = res['time']
        number_tNew = tOld[-1]//dt
        tNew = np.arange(0, number_tNew)*dt
        
        res['time'] = tNew
        res['surface_elevation'] = np.interp(tNew, tOld, res['surface_elevation'])
        res['surface_elevation_derivative_etax'] = np.interp(tNew, tOld, res['surface_elevation_derivative_etax'])
        res['surface_elevation_derivative_etay'] = np.interp(tNew, tOld, res['surface_elevation_derivative_etay'])

        res['velocity_u'] = interp2D(res, 'velocity_u')
        res['velocity_v'] = interp2D(res, 'velocity_v')
        res['velocity_w'] = interp2D(res, 'velocity_w')

        res['position_x'] = interp2D(res, 'position_x')        
        res['position_y'] = interp2D(res, 'position_y')        
        res['position_z'] = interp2D(res, 'position_z')        

        res['velocity_derivative_ux'] = interp2D(res, 'velocity_derivative_ux')        
        res['velocity_derivative_uy'] = interp2D(res, 'velocity_derivative_uy')        
        res['velocity_derivative_uz'] = interp2D(res, 'velocity_derivative_uz')        

        res['velocity_derivative_vx'] = interp2D(res, 'velocity_derivative_vx')        
        res['velocity_derivative_vy'] = interp2D(res, 'velocity_derivative_vy')        
        res['velocity_derivative_vz'] = interp2D(res, 'velocity_derivative_vz')        

        res['velocity_derivative_wx'] = interp2D(res, 'velocity_derivative_wx')        
        res['velocity_derivative_wy'] = interp2D(res, 'velocity_derivative_wy')        
        res['velocity_derivative_wz'] = interp2D(res, 'velocity_derivative_wz')        

        res['velocity_derivative_ut'] = interp2D(res, 'velocity_derivative_ut')        
        res['velocity_derivative_vt'] = interp2D(res, 'velocity_derivative_vt')        
        res['velocity_derivative_wt'] = interp2D(res, 'velocity_derivative_wt')    

    
    # Concantenate all the results in a single dictionary
    finalResult = {}
    if len(allResults) > 0:
        for key_ in allResults[0].keys():
            finalResult[key_] = np.concatenate([ar[key_] for ar in allResults])
    else:
        finalResult = allResults[0]

    finalResult['time'] = np.arange(len(finalResult['time']))*dt

    # Now remove the part exceeding the required length
    tNew = finalResult['time'][:]
    F = tNew < timeSeriesLength
    for key_ in finalResult.keys():
        finalResult[key_] = finalResult[key_][F]

    # Write a .mat file
    if format == 'mat':
        sio.savemat(os.path.join(outputFolder, 'Kinematics{:02d}.mat'.format(i)), finalResult)

    # Write and hdf5 file
    elif format == 'h5':
        fid = h5py.File(os.path.join(outputFolder, 'Kinematics{:02d}.h5'.format(i)), 'w')
        for i,key_ in enumerate(finalResult.keys()):
            fid.create_dataset(key_, data=finalResult[key_])
        fid.close()

    # Write a .ascii file
    elif format == 'ascii':
        raise Exception('Ascii file writer not implemented yet!')
            

def breaking_limit(H=None, depth=None, T=None, g=9.80665):

    ''' Calculate breaking limit for a regular wave
    Parameters:
    -----------

    H : float
        Wave height [m]
    depth : float
        Depth [m]
    T : float
        Wave period
    g : float
        Gravity

    Returns:
    -------
    ratio: float
        Ratio to breaking limit
    Hb : float
        Breaking wave height
    '''


    dl0 = np.array([2,0.578,0.440,0.356,0.293,0.243,0.201,0.166,0.1359,
              0.1100,0.0876,0.0686,0.0524,0.0390,0.0277,0.01879,
              0.01168,0.00638])
    hl0 = np.array([0.1682,0.1665,0.1613,0.1531,0.1423,0.1298,0.1159,
             0.1017,0.0873,0.0735,0.0605,0.0487,0.0380,0.0289,
             0.0208,0.01440,0.00911,0.00501])

    el0 = g*T**2/(2*np.pi)
    ha =  H / el0
    da = depth / el0
    if (da >= dl0[0]):
        ratio = ha / hl0[0]
        hb = hl0[0]
    elif (da < dl0[17]):
        ratio = ha / (0.8 * da)
        hb = 0.8*da
    else:
        for i in range(1,18):
            if (dl0[i] < da):
                x1 = np.log(dl0[i])
                x2 = np.log(dl0[i-1])
                y1 = np.log(hl0[i])
                y2 = np.log(hl0[i-1])
                r = (np.log(da)-x1)/(x2-x1)
                hb = np.exp(y1+r*(y2-y1))
                ratio = ha / hb

                break

    Hb = hb * el0

    return Hb
